# LEIA-ME #

Este arquivo contém instruções para você provar pra gente que não existe ninguém melhor do que você pra trabalhar na Bulk Design! :-)

### REQUISITOS MÍNIMOS ###

* Não ligamos para a sua escolaridade. Conhecemos gente que é formada nas melhores universidades e não tem TALENTO nem CRIATIVIDADE.
* Hoje em dia, e principalmente na área da tecnologia, é indispensável saber inglês. Não precisa ser fluente, apenas ter uma boa leitura já seria ótimo.
* Nós somos amantes do Design, isso está no nosso nome. Por isso precisamos de alguém que também tenha a mesma paixão por soluções visuais que impactam. Visite o nosso site e veja como gostamos do clean.
* Trabalhamos bastante com CMS, então é muito importante que você tenha conhecimento em PHP, HTML e CSS (principalmente os dois últimos).
* Ter uma noção de Git seria legal pra caramba.
* Ainda falando em CSS, a gente usa muito os frameworks MaterializeCSS e às vezes o Bootstrap. Então, se você tem conhecimento em um dos dois o seu nome será gravado mais facilmente. #ficadica 

###  PERFIL DO CANDIDATO ###

* Tudo bem se você não souber tudo o que foi dito nos requisitos. Nós também aprendemos a cada dia um pouquinho mais. Por isso mesmo insistimos que você tenha VONTADE DE APRENDER e esteja aberto para receber sugestões de melhoria e é claro, elogios. Aqui ninguém é melhor do que ninguém, e nunca será!
* Como dissemos nos requisitos, não importa o seu grau de escolaridade. Mas isso não significa que não seja fundamental estar por dentro do que acontece *around the world*, até porque isso influencia (e muito) no processo criativo do designer.
* Queremos alguém que não trabalhe apenas pelo dinheiro, e sim abrace o projeto e pense que as coisas podem dar tão certo que um dia a equipe estará rindo em um iate. Não custa sonhar. :)

### SOBRE A BULK DESIGN ###

* A Bulk Design iniciou suas atividades em 2016 e tem como objetivos a valorização das pessoas e a integridade, porque não adianta vender e não entregar um trabalho que *realmente* resolva o problema do cliente. Nada pior do que vender gato por lebre, né non?
* Em 2017 a Bulk começou a decolar, por essa razão estamos precisando de alguém que entre junto na nave e nos ajude a decolar. Observação: você irá decolar com a gente, e não ficar olhando de longe! Todos a bordo.

### OK, CHEGA DE CONVERSA E MÃOS À OBRA! ###

* Já que estamos falando de uma vaga para a área Web, por que você não mostra o que sabe? ;)

* Queremos que você desenvolva uma página em HTML (utilizando um dos frameworks de CSS), mostrando pra gente porquê devemos te chamar para o time. Não importa o tanto de páginas ou os efeitos que você pretende usar.
* Lembre-se que nós amamos design clean, que possa mostrar de forma LINDA a razão pela qual você é o the best.

* O repositório do BitBucket está com uma pasta seguindo a seguinte divisão:

* - index.html
* -- CSS
* --- bootstrap.css
* --- materialize.css
* -- FONTS
* -- JS
* --- bootstrap.js
* --- materialize.js

* Quando você terminar de desenvolver a página, envie um e-mail para *hello@bulkdesign.com.br* com o seguinte assunto: “Join the Bulk - *SEU NOME* - Permissão para decolar!”. 

* Após recebermos seu e-mail enviaremos um acesso para você publicar a página desenvolvida, e mesmo se você não for selecionado nós te avisaremos. Nada pior do que ficar na expectativa e acabar ficando no vácuo!

BOA SORTE!